#include <iostream>
#include <sstream>
#include "my_serial.hpp"

using namespace my_serial;

int main(int argc, char **argv) {
    auto *ctx = new serial_t;
    if (serial_init(ctx, "/dev/cu.usbserial-140", 9600, 'N', 8, 1) == -1) {
        std::cerr << "Error serial init: " << strerror(errno) << std::endl;
        return -1;
    }

    if (serial_open(ctx) == -1) {
        std::cerr << "Error serial open: " << strerror(errno) << std::endl;
        return -1;
    }

    u_int8_t src[] = {0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};
    static uint8_t dst[32];

    size_t n_write = write_com(ctx, src, sizeof src, 100);
    std::cout << n_write << std::endl;

    size_t n_read = read_com(ctx, dst, sizeof dst, 100000);
    std::cout << n_read << std::endl;

    serial_close(ctx);
    serial_free(ctx);

    for (size_t i = 0; i < n_read; i++) {
        std::cout << (char)dst[i];
    }

    //std::cout << (uint16_t)'\r' << std::endl;
    //std::cout << (uint16_t)'\n' << std::endl;

    return 0;
}
