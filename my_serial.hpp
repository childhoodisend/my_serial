//
// Created by pahpan on 28.07.2022.
//

#ifndef TEST_MY_SERIAL_HPP
#define TEST_MY_SERIAL_HPP

#include <cstdio>      // standard input / output functions
#include <cstdlib>
#include <cstring>     // string function definitions
#include <unistd.h>    // UNIX standard function definitions
#include <fcntl.h>     // File control definitions
#include <cerrno>      // Error number definitions
#include <termios.h>   // POSIX terminal control definitions
#include <string>
#include "poll.h"

namespace my_serial {

    struct serial_t {
        int fd = -1;
        bool debug = true;
        /* Device: "/dev/ttyS0", "/dev/ttyUSB0" or "/dev/tty.USA19*" on Mac OS X. */
        char *device = nullptr;
        /* Bauds: 9600, 19200, 57600, 115200, etc */
        int baud = B9600;
        /* Data bit */
        uint8_t data_bit = 0x08;
        /* Stop bit */
        uint8_t stop_bit = 0x01;
        /* Parity: 'N', 'O', 'E' */
        char parity = 'N';

        /* Save old termios settings */
        struct termios old_tty{};
    };

    void serial_free(serial_t *ctx) {
        if (ctx == nullptr) {
            errno = ENOMEM;
            return;
        }

        delete[] ctx->device;
        delete ctx;
    };

    int serial_init(serial_t *ctx, const char *device, int baud, char parity, int data_bit, int stop_bit) {
        /* Check device argument */
        if (device == nullptr || *device == 0) {
            fprintf(stderr, "The device string is empty\n");
            errno = EINVAL;
            return -1;
        }

        /* Check baud argument */
        if (baud == 0) {
            fprintf(stderr, "The baud rate value must not be zero\n");
            errno = EINVAL;
            return -1;
        }

        /* Device name and \0 */
        ctx->device = new char[strlen(device) + 1];
        if (ctx->device == nullptr) {
            serial_free(ctx);
            errno = ENOMEM;
            return -1;
        }
//        ctx->device = device;
        strcpy(ctx->device, device);
        ctx->baud = baud;

        if (parity == 'N' || parity == 'E' || parity == 'O') {
            ctx->parity = parity;
        } else {
            serial_free(ctx);
            errno = EINVAL;
            return -1;
        }
        ctx->data_bit = data_bit;
        ctx->stop_bit = stop_bit;

        return 0;
    }

    int serial_open(serial_t *ctx) {
        if (ctx == nullptr) {
            errno = ENOMEM;
            return -1;
        }

        int fd;
        struct termios tty{}, old_tty{};
        speed_t speed;

        fd = open(ctx->device, O_RDWR | O_NOCTTY);
        if (fd == -1) {
            fprintf(stderr, "ERROR Can't open the device %s (%s)\n", ctx->device, strerror(errno));
            serial_free(ctx);
            close(fd);
            return -1;
        }
        ctx->fd = fd;

        if (tcgetattr(fd, &old_tty) < 0) {
            fprintf(stderr, "ERROR Can't get attr (%s)\n", strerror(errno));
            serial_free(ctx);
            close(fd);
            return -1;
        }

        ctx->old_tty = old_tty;
        tty = old_tty;

        switch (ctx->baud) {
            case 4800:
                speed = B4800;
                break;
            case 9600:
                speed = B9600;
                break;
            case 19200:
                speed = B19200;
                break;
            case 38400:
                speed = B38400;
                break;
            case 57600:
                speed = B57600;
                break;

            default:
                speed = B9600;
        }

        /* Set the baud rate */
        if (cfsetspeed(&tty, speed) < 0) {
            serial_free(ctx);
            close(fd);
            return -1;
        }

        switch (ctx->data_bit) {
            case 5:
                tty.c_cflag |= CS5;
                break;
            case 6:
                tty.c_cflag |= CS6;
                break;
            case 7:
                tty.c_cflag |= CS7;
                break;
            case 8:
            default:
                tty.c_cflag |= CS8;
                break;
        }

        /* Stop bit (1 or 2) */
        if (ctx->stop_bit == 1)
            tty.c_cflag &= ~CSTOPB;
        else /* 2 */
            tty.c_cflag |= CSTOPB;


        /* PARENB       Enable parity bit
           PARODD       Use odd parity instead of even */
        if (ctx->parity == 'N') {
            /* None */
            tty.c_cflag &= ~PARENB;
        } else if (ctx->parity == 'E') {
            /* Even */
            tty.c_cflag |= PARENB;
            tty.c_cflag &= ~PARODD;
        } else {
            /* Odd */
            tty.c_cflag |= PARENB;
            tty.c_cflag |= PARODD;
        }

        tty.c_cc[VMIN] = 0;
        tty.c_cc[VTIME] = 10;

        /* Make raw */
        cfmakeraw(&tty);

        /* Flush Port, then applies attributes */
        tcflush(fd, TCIFLUSH);
        if (tcsetattr(fd, TCSANOW, &tty) < 0) {
            serial_free(ctx);
            close(fd);
            return -1;
        }

        return fd;
    }

    void serial_close(serial_t *ctx) {
        if (ctx == nullptr) {
            errno = ENOMEM;
            return;
        }

        if (ctx->fd != -1) {
            tcsetattr(ctx->fd, TCSANOW, &ctx->old_tty);
            close(ctx->fd);
            ctx->fd = -1;
        }
    }

    size_t read_com(serial_t *ctx, uint8_t *src, size_t size, int timeout) {
        size_t ret = 0;

        pollfd fds{};
        fds.fd = ctx->fd;
        fds.events = POLLIN;

        poll(&fds, 1, timeout);
        if (fds.revents & POLLIN) {
            // TODO
            usleep(9600 / 10 * size * 8);
            ret = read(ctx->fd, src, size);
        }
        tcflush(ctx->fd, TCIFLUSH);
        return ret < 0 ? 0 : ret;
    }

    size_t write_com(serial_t *ctx, uint8_t *dst, size_t size, int timeout) {
        if (ctx == nullptr) {
            serial_free(ctx);
            errno = ENOMEM;
            return -1;
        }

        size_t ret = 0;

        pollfd fds{};
        fds.fd = ctx->fd;
        fds.events = POLLOUT;

        poll(&fds, 1, timeout);
        if (fds.revents & POLLOUT) {
            ret = write(ctx->fd, (uint8_t *) dst, size);
            tcdrain(ctx->fd);
        }
        tcflush(ctx->fd, TCOFLUSH);
        return ret != size ? 0 : ret;
    }

}
#endif //TEST_MY_SERIAL_HPP
